function Pokemon(name, lvl, hp){
	
	this.name   = name;
	this.lvl    = lvl;
	this.health = hp * 2;
	this.attack = lvl;

	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}.`);
		target.health -= this.attack;
		console.log(`${target.name}'s hp is now reduced to ${target.health}.`);
		if (target.health <= 10) {
			target.faint();
		};
	};
	this.faint = function(){
		console.log(`${this.name} fainted.`)
	};
};

let jiggly = new Pokemon("Jigglypuff", 10, 25);
let snorlax = new Pokemon("Snorlax", 10, 25);

jiggly.tackle(snorlax);
snorlax.tackle(jiggly);
jiggly.tackle(snorlax);
snorlax.tackle(jiggly);
jiggly.tackle(snorlax);
snorlax.tackle(jiggly);
snorlax.tackle(jiggly);