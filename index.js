// A variable with mulitple values
	// a: array
		let cartoons = ["Elmo", "Mickey", "Daisy", "Barbie"]

// JavaScript Objects
	// Similar to an array, it also contains multiple values.
	// Unlike an array that uses indexes, objects use properties.
	// With objects, we can easily give labels to each value.

	// Structure or Syntax
		/*
			let objectName = {
				key : value,
				key : (function),
				key : object{
					key : value
				},
				key : [array]
			}
		*/

		let cellphone = {
			name : "Nokia 3210",
			manufactureDate : 1999
		};

		console.log(cellphone);
		console.log(typeof cellphone);

	// Creating Objects Using a Constructor Function
		/*
			Syntax:

			function objectName(keyA, keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			};
		*/

		function Laptop(name, manufactureDate){
			this.name = name;
			this.releaseDate = manufactureDate;
		};

		let laptop = new Laptop("Lenovo", 2008);
		console.log(laptop);
			// will create new object.
			// The new operator creates an instance of an object.

		let laptop1 = Laptop("Asus", 2010);
		console.log(laptop1);
			// result: undefined
			// (Lack of new operator creates undefined.)

	// Creating Empty Objects

		let computer = {};
		let myComputer = new Object();
		console.log(computer);
		console.log(myComputer);

	// Accessing Array Objects

		let machines = [laptop, laptop1];

		// One way to access the property of an object inside an array:
		console.log(machines[0].name);
			// result: Lenovo
		// Another way of accessing properties in an array.
		console.log(machines[0]["releaseDate"]);
			// result: 2008

	// Initializing/Adding/Deleting/Reassigning Object Properties

		let car = {};

		// initializing/adding object properties using dot notation
		car.name = "Honda Civic";
		console.log(car);

		// adding object properties with square brackets
		car["manufactureDate"] = 2019;
		console.log(car);

		// deleting object properties
		delete car["name"];
		console.log(car);

		car.brand = "Honda";
		car.model = "Civic";
		car.color = "black";
		car.location = "PH";
		console.log(car);

		// Reassigning object properties
		car.manufactureDate = 1986
		console.log(car);

	// Object Methods
		// A method is a function which is a property of an object
		// They are also functions, and one of the key differences they have is that methods are functions related to a specific object.
		// Methdos are defined based on what an object is capable of doing and how it should work.
		// (When making functions in an object, the key becomes the name of the function.)

		let person = {
			name : "Jin",
			talk : function(){
				console.log(`Hello, my name is ${this.name}.`);
			}
		};

		person.talk();

		let friend = {
			firstName : "Bam",
			lastName  : "Jeon",
			address   : {
				city    : "Caloocan",
				country : "Philippines"
			},
			emails    : ["bam@mail.com","jeonbam@mail.com"],
			introduce : function(){
				console.log(`Hello, my name is ${this.firstName} ${this.lastName}. I live in ${this.address.city}. My email is ${this.emails[0]}.`)
			}
		};
		friend.introduce();

		// Real World Application
			// Scenario: 
				// 1. Create a game that would have several pokemons to interact with each other.
				// 2. Every pokemon should have stats, properties, and functions.

			let myPokemon = {
				name   : "Bulbasaur",
				level  : 3,
				health : 100,
				attack : 50,
				tackle : function(){
					console.log(`${this.name} tackled another pokemon.`);
					console.log(`targetPokemon's health is now reduced.`);
				},
				faint  : function(){
					console.log(`${this.name} fainted.`);
				},
			};
			myPokemon.tackle();
			myPokemon.faint();

			// Creating object with an object constructor

			function Pokemon(name, lvl, hp){
				// Properties
				this.name   = name;
				this.lvl    = lvl;
				this.health = hp * 2;
				this.attack = lvl;

				// Methods
				this.tackle = function(target){
					console.log(`${this.name} tackled ${target.name}`);
					console.log(`targetPokemon's health is now reduced.`);
					console.log(target.name);
					console.log(this.name);
				};
				this.faint = function(){
					console.log(`${this.name} fainted.`)
				}
			};

			// Create new instances of the Pokemon object each with their unique properties.

			let pikachu = new Pokemon("Pikachu", 3, 50);
			let ratata = new Pokemon("Ratata", 5, 25);

			// Providing the "ratata" object as an argument to "pikachu" tackle method will create interaction between two objects.

			pikachu.tackle(ratata);
			ratata.tackle(pikachu);
			